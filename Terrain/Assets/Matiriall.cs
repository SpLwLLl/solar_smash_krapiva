﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Matiriall : MonoBehaviour
{
    public Material Material1;
    public Material Material2;

    Renderer rend;
    public float duration = 5.0f;

    public void Start()
    {
        rend = GetComponent<Renderer>();
    }

   
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Yellow")
        {
            float lerp1 = Mathf.PingPong(Time.time, duration);
            rend.material.Lerp(Material1, Material2, lerp1);
        }
    }

}

