﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trigger : MonoBehaviour
{
    public GameObject Box;
    public Transform SpawnPoint;

    public GameObject ObgTrig;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            {
                Instantiate(Box, SpawnPoint.position, Quaternion.identity);
            }
        }

    }

}
