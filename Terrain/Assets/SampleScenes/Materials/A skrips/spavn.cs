﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spavn : MonoBehaviour
{

    public float Time1;
    public float Skeletnom = 0;
    public GameObject Skeleton;
    public Transform SpawnPoint;
   

    private void Update()
    {
        Time1 += Time.deltaTime;
        if (Time1 >= 5 & Skeletnom <= 1)
        {
            Instantiate(Skeleton, SpawnPoint.position, Quaternion.identity);
            Time1 = 0;
            Skeletnom += 1;
        }
    }

}
