﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelowWord : MonoBehaviour
{
    public GameObject flashlight;
    public bool On;


    // Start is called before the first frame update
    void Start()
    {
        flashlight.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z) & On == false)
        {

            flashlight.SetActive(true);
            On = true;
        }
        else if (Input.GetKeyDown(KeyCode.Z) & On == true)
        {

            flashlight.SetActive(false);
            On = false;

        }
    }   
}
