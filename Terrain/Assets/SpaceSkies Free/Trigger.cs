﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    public float Increase;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Yellow") 
        {
            Debug.Log("Trigger On");
            transform.localScale = new Vector3(Increase, Increase, Increase) * Time.deltaTime;
        }
    }
}
