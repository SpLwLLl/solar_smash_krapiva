﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Valve.VR;
using Valve.VR.InteractionSystem;

public class PlayerRay_Green : MonoBehaviour
{

    
    public SteamVR_Action_Boolean grabPinchAction = SteamVR_Input.GetAction<SteamVR_Action_Boolean>("GrabPinch");

    void Update()
    {
        
        Ray ray = new Ray(transform.position, transform.forward);
        Debug.DrawRay(transform.position, transform.forward * 100, Color.yellow);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.collider.gameObject.GetComponent<Selectab>())
            {
                hit.collider.gameObject.GetComponent<Selectab>().Select();
                if (grabPinchAction.stateDown)
                {

                }
            }
        }
        else
        {
            hit.collider.gameObject.GetComponent<Selectab>().Deselect();
        }
    }
}
